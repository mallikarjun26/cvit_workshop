main

This program takes 2 inputs. First argument is the path for an image file and second is the path for output text file. It detects any faces present in the image provided and writes the rectangular co-ordinates into the text file provided.
