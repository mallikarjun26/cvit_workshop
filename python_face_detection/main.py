import sys 
import numpy as np
import cv2

def main(argv):

    face_cascade = cv2.CascadeClassifier('haarcascade_frontalface_default.xml')
    f = open(sys.argv[2], 'w')
    
    img = cv2.imread(sys.argv[1])
    gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    
    faces = face_cascade.detectMultiScale(gray, 1.3, 5)
    for (x,y,w,h) in faces:
        f.write(str(x) + ' ' + str(y) + ' ' + str(w) + ' ' + str(h) + '\n')
        cv2.rectangle(img,(x,y),(x+w,y+h),(255,0,0),2)
    
    cv2.imshow('img',img)
    cv2.waitKey(0)
    cv2.destroyAllWindows()

if __name__ == "__main__":
    main(sys.argv[1:])
